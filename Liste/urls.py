from django.contrib import admin
from django.urls import path, include
from shopping import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('shopping.urls')),
]