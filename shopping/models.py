from django.db import models

class List(models.Model):
	item = models.TextField(blank=False, max_length=200)
	menge = models.TextField(blank=True, max_length=200)
	completed = models.BooleanField(default=False) 
	"""default ist 'False' da das Item noch nicht 'abgehakt' wurde"""

	def __str__(self):
		return self.item + self.menge + '  ' + str(self.completed)
