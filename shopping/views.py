from django.shortcuts import render, redirect
from shopping.models import List
from shopping.forms import ListForm
from django.contrib import messages

def index(request):
	if request.method == 'POST':
		form = ListForm(request.POST or None)

		if form.is_valid():
			form.save()
			all_items = List.objects.all
			messages.success(request, ("Item wurde zur Einkaufsliste hinzugefügt!"))
			return render(request, 'index.html', {'all_items': all_items})

	else:
		all_items = List.objects.all
		return render(request, 'index.html', {'all_items': all_items})

def delete(request, list_id):
	item = List.objects.get(pk=list_id)
	item.delete()
	messages.success(request, ('Item wurde aus der Einkaufsliste entfernt!'))
	return redirect('index')

def cross_off(request, list_id):
	item = List.objects.get(pk=list_id)
	item.completed = True
	item.save()
	messages.success(request, ('Item wurde als erledigt markiert!'))
	return redirect('index')

def uncross(request, list_id):
	item = List.objects.get(pk=list_id)
	item.completed = False
	item.save()
	messages.success(request, ('Item wurde als nicht erledigt markiert.'))
	return redirect('index')